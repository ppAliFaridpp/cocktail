from django.test import TestCase
from rest_framework.exceptions import ErrorDetail

from core import models as core_models
from core import serializers as core_serializers


class DefaultCocktails(TestCase):
    default_cocktails_count = 3

    def test_number_of_cocktails(self):
        self.assertGreaterEqual(core_models.Cocktail.objects.count(), self.default_cocktails_count)


class CocktailTableSerializerTestCase(TestCase):
    def setUp(self) -> None:
        self.cocktail1 = core_models.Cocktail.objects.create(label='Apple')
        core_models.Ingredient.objects.create(name='water', volume=20, cocktail=self.cocktail1)
        core_models.Ingredient.objects.create(name='sugar', volume=2, cocktail=self.cocktail1)
        core_models.Ingredient.objects.create(name='apple', volume=8, cocktail=self.cocktail1)

        self.cocktail2 = core_models.Cocktail.objects.create(label='NoWater')
        core_models.Ingredient.objects.create(name='sugar', volume=4, cocktail=self.cocktail2)
        core_models.Ingredient.objects.create(name='orange', volume=10, cocktail=self.cocktail2)

    def test_valid(self):
        ser_cocktail1 = core_serializers.CocktailSerializer(self.cocktail1)
        self.assertTrue(ser_cocktail1.data)
        cocktail_table = core_serializers.CocktailSerializer(data=ser_cocktail1.data)
        self.assertTrue(cocktail_table.is_valid(raise_exception=False))

    def test_invalid(self):
        ser_cocktail = core_serializers.CocktailSerializer(self.cocktail2)
        self.assertTrue(ser_cocktail.data)
        cocktail_table = core_serializers.CocktailTableSerializer(data=ser_cocktail.data)
        self.assertFalse(cocktail_table.is_valid(raise_exception=False))
        self.assertEqual(cocktail_table.errors,
                         {'ingredients': [ErrorDetail(string='water should be in ingredients', code='invalid')]})

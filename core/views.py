from rest_framework import (
    generics, permissions
)
from rest_framework.response import Response
from rest_framework import status

from core import serializers as core_serializers
from core import models as core_models


class CocktailListAPIView(generics.ListAPIView):
    permissions = (permissions.AllowAny,)
    serializer_class = core_serializers.CocktailSerializer
    queryset = core_models.Cocktail.objects.all()

    def list(self, request, *args, **kwargs):
        """
        gets the queryset of cocktails and then serializes it into the table representation
        """
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        table_serializer = core_serializers.CocktailTableSerializer(data=serializer.data, many=True)
        table_serializer.is_valid(raise_exception=True)
        return Response(table_serializer.data, status=status.HTTP_200_OK)


class AddWaterView(generics.GenericAPIView):
    permission_classes = (permissions.AllowAny,)
    serializer_class = core_serializers.CocktailTableSerializer

    def add_water(self):
        cocktail = self.request.data.get('cocktail')
        volume = self.request.data.get('volume')
        for i in cocktail['ingredients']:
            if i['name'] == 'water':
                i['volume'] += float(volume)
        return cocktail

    def post(self, *args, **kwargs):

        # validate cocktail table
        serializer = self.get_serializer(data=self.request.data.get('cocktail'))
        serializer.is_valid(raise_exception=True)

        # add water
        new_cocktail = self.add_water()

        # validate and serializer the new table
        new_serializer = self.get_serializer(data=new_cocktail)
        new_serializer.is_valid(raise_exception=True)

        return Response(new_serializer.data, status=status.HTTP_200_OK)

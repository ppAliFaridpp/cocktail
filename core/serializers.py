from rest_framework import serializers
from rest_framework import exceptions

from core import models as core_models


class IngredientSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length=25, required=True)
    volume = serializers.FloatField(required=True)

    class Meta:
        model = core_models.Ingredient
        fields = ('name', 'volume')


class CocktailTableSerializer(serializers.Serializer):
    """
    Main serializer of cocktail tables.
    Table must contain sugar and water.
    The other fields are optional and goes into a list.

    example of a valid data:

    {
        "label": "Apple Juice",
        "total_sum": 50,
        "fruit_sum": 10,
        "ingredients":
            [
                {
                    "name": "water",
                    "volume": 35,
                },
                {
                    "name": "sugar",
                    "volume": 5,
                },
                {
                    "name": "Apple",
                    "volume": 10,
                }
            ]
    }

    """

    required_ingredients = ['water', 'sugar']

    label = serializers.CharField(max_length=50, required=True)
    ingredients = IngredientSerializer(many=True)
    total_sum = serializers.SerializerMethodField(read_only=True)
    fruit_sum = serializers.SerializerMethodField(read_only=True)

    def get_total_sum(self, obj):
        return sum([i.get('volume') for i in obj.get('ingredients')])

    def get_fruit_sum(self, obj):
        volume = sum(
            [i.get('volume') for i in obj.get('ingredients') if i.get('name') not in self.required_ingredients])
        percentage = (volume / self.get_total_sum(obj)) * 100
        return {
            'volume': volume,
            'percentage': round(percentage, 2)
        }

    @staticmethod
    def cal_ingredient_percentage(volume, total):
        return round((volume / total) * 100, 2)

    def add_ingredients_percentage(self, rep):
        ingredients = rep.get('ingredients')
        total_sum = rep.get('total_sum')
        for ing in ingredients:
            ing['percentage'] = self.cal_ingredient_percentage(ing['volume'], total_sum)

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        self.add_ingredients_percentage(representation)
        return representation

    def validate_ingredients(self, ingredients):
        ingredients_name = [i['name'] for i in ingredients]
        for i in self.required_ingredients:
            if i not in ingredients_name:
                raise exceptions.ValidationError('{} should be in ingredients'.format(i))
        return ingredients


class CocktailSerializer(serializers.ModelSerializer):
    ingredients = IngredientSerializer(many=True)

    class Meta:
        model = core_models.Cocktail
        fields = ('label', 'ingredients')

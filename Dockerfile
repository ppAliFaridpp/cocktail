FROM python:3.8

RUN mkdir /opt/app
WORKDIR /opt/app
ADD requirements.txt .

RUN pip3 install -r requirements.txt

ADD . .
